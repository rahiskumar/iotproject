package me.codeandroid.bulbapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.bulbapp.database.DatabaseHelper;

public class MainActivity extends AppCompatActivity implements MqttCallback, View.OnTouchListener {
    String TAG = "MainActivity";
    ViewGroup imgLayout;
    private long lastTouchDown;
    private int CLICK_ACTION_THRESHHOLD = 200;
    List<ImageView> imageList;
    int windowwidth; // Actually the width of the RelativeLayout.
    int windowheight; // Actually the height of the RelativeLayout.
    float dX ;
    float dY;
    private int _xDelta;
    private int _yDelta;
    DatabaseHelper databaseHelper;
    int i = 1;
    MqttAndroidClient client;
    SeekBar seekBar;
    Button closeBtn;
    ImageView redImageView, greenImageView;
    int seekBarValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHelper = new DatabaseHelper(this);
        imgLayout = (ViewGroup) findViewById(R.id.rootLayout);
        imageList = new ArrayList<ImageView>();
        if (imageList.size() > 0){
            imageList.clear();
        }

        int size = databaseHelper.getDevices();

        for (i = 1; i <= size; i++){
            String topic;
            ImageView imageView = new ImageView(MainActivity.this);

            imageView.setLayoutParams(new RelativeLayout.LayoutParams(120,120));
            imageView.setId(i);
            imageView.setTag(i);
            imageView.setX(databaseHelper.getXposition(i));
            imageView.setY(databaseHelper.getYposition(i));
            int vValue = databaseHelper.getVvalue(i);
            Log.d("vValueInLoop", String.valueOf(vValue));
            topic = databaseHelper.getTopic(i);
            if (databaseHelper.getVValue(topic).equals("1")){
                imageView.setImageResource(R.drawable.ic_lightbulb_outline_yellow_24dp);
            }else {
                imageView.setImageResource(R.drawable.ic_lightbulb_outline_black_24dp);
            }
            //Log.d("topic", topic+ " " +String.valueOf(i));
            //   imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            imageView.setOnTouchListener(this);
            imgLayout.addView(imageView);
            imageList.add(imageView);


        Log.d("imageListSize", String.valueOf(imageList.size()));

        //MQTTConnect options : setting version to MQTT 3.1.1
        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
       /* options.setUserName("your_mqtt_username_here");
        options.setPassword("your_mqtt_password_here".toCharArray());
*/
        //Below code binds MainActivity to Paho Android Service via provided MqttAndroidClient
        // client interface
        //Todo : Check why it wasn't connecting to test.mosquitto.org. Isn't that a public broker.
        //Todo : .check why client.subscribe was throwing NullPointerException  even on doing subToken.waitForCompletion()  for Async                  connection estabishment. and why it worked on subscribing from within client.connect’s onSuccess(). SO
        String clientId = MqttClient.generateClientId();
        client =
                new MqttAndroidClient(this.getApplicationContext(), "tcp://broker.mqttdashboard.com:1883",
                        clientId);


        try {
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d(TAG, "onSuccess");
                    Toast.makeText(MainActivity.this, "Connection successful", Toast.LENGTH_SHORT).show();
                    //Subscribing to a topic door/status on broker.hivemq.com
                    client.setCallback(MainActivity.this);
                   // final String topic = databaseHelper.getTopic(i);
                    Log.d("Topic", topic);
                    int qos = 1;
                    try {
                        IMqttToken subToken = client.subscribe(topic, qos);
                        subToken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                // successfully subscribed
                                Toast.makeText(MainActivity.this, "Successfully subscribed to: " + topic, Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken,
                                                  Throwable exception) {
                                // The subscription could not be performed, maybe the user was not
                                // authorized to subscribe on the specified topic e.g. using wildcards
                                Toast.makeText(MainActivity.this, "Couldn't subscribe to: " + topic, Toast.LENGTH_SHORT).show();

                            }
                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d(TAG, "onFailure");
                    Toast.makeText(MainActivity.this, "Connection failed", Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception e){
            Log.d("e", e.toString());
        }
        }
        imgLayout.post(new Runnable() {
            @Override
            public void run() {
                windowwidth = imgLayout.getWidth();
                windowheight = imgLayout.getHeight();
            }
        });
    }


    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        String []separated = topic.split("/");
        String applianceNumber = separated[2];
        Log.d("topicSeparated", topic + " " + separated[2]);
        int index = Integer.parseInt(applianceNumber) - 1; //TODO index is the identity of the topic and appliance number
        /*
         * To test ,publish  "open"/"close" at topic you subscibed app to in above .
         * */
        //ImageView doorImage = (ImageView)findViewById(R.id.door_image);
        JSONObject object = new JSONObject(String.valueOf(message));

        imageList.get(index).getId();

        Log.d("door",message.toString());

        int vValue = object.getInt("V");
        int dValue = object.getInt("D");

        Log.d("Int VD", String.valueOf(vValue)+ " || "+ String.valueOf(dValue));

        String updateQuery = "update device set currentV = "+vValue+", currentD = "+dValue+" where topic = '"+topic+"'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.execSQL(updateQuery);

        Log.d("updateV&DMain", updateQuery);

        if(String.valueOf(vValue).equals("0")){
          //  doorImage.setImageResource(R.drawable.ic_lightbulb_outline_black_24dp);
            imageList.get(index).setImageResource(R.drawable.ic_lightbulb_outline_black_24dp);

        }
        else {
         //   doorImage.setImageResource(R.drawable.ic_lightbulb_outline_yellow_24dp);
            imageList.get(index).setImageResource(R.drawable.ic_lightbulb_outline_yellow_24dp);

        }

        Toast.makeText(MainActivity.this, "Topic: "+topic+"\nMessage: "+message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int viewId = view.getId();
        Log.d("viewId", String.valueOf(viewId));
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        int width = view.getLayoutParams().width;
        int height = view.getLayoutParams().height;


        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                // _xDelta and _yDelta record how far inside the view we have touched. These
                // values are used to compute new margins when the view is moved.
                lastTouchDown = System.currentTimeMillis();
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                _xDelta = X - view.getLeft();
                _yDelta = Y - view.getTop();

                break;
            case MotionEvent.ACTION_UP:

                String query = "update device set xPosition = "+X+", yPosition = "+Y+" where id = "+view.getId()+"";
                SQLiteDatabase db = databaseHelper.getWritableDatabase();
                db.execSQL(query);
                Log.d("updatePositions", query);

                //TODO this is part to work now
                //save icon position in db
                final String topic = databaseHelper.getTopic(view.getId());

                if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {
                    Log.w("App", "You clicked!");
                    if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {
                        Log.w("App", "You clicked!");

                        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(this);
                        LayoutInflater inflater = getLayoutInflater();
                        View view1 = inflater.inflate(R.layout.dimmer_layout, null);
                        alertbuilder.setView(view1);
                        AlertDialog alertDialog = alertbuilder.create();
                        seekBar = (SeekBar) view1.findViewById(R.id.dimmerseekBar);
                        //closeBtn = (Button) view1.findViewById(R.id.closeBtn);

                        redImageView = (ImageView) view1.findViewById(R.id.redImg);
                        greenImageView = (ImageView) view1.findViewById(R.id.greenImg);

                        int min = 1;
                        final int max = 255;

                        seekBar.setMax(max - min);

                        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                seekBarValue= max - i;
                                Toast.makeText(MainActivity.this, ""+String.valueOf(seekBarValue), Toast.LENGTH_SHORT).show();
                                Log.d("va;ue", String.valueOf(seekBarValue));

                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });


                        redImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int vValue = databaseHelper.getVvalue(view.getId());
                                int dValue = databaseHelper.getDvalue(view.getId());
                                    int newVValue = 0;
                                    publishMessage(newVValue, dValue, topic);
                                    String sql = "update device set currentV =" + newVValue + ",";
                                    sql += "currentD =" + seekBarValue + "";
                                    sql += " where  topic='" + topic + "'";
                                    SQLiteDatabase db = databaseHelper.getWritableDatabase();
                                    db.execSQL(sql);

                                    Log.d("publishMessageMain", String.valueOf(newVValue)+" "+ String.valueOf(dValue)+ " " +topic);

                                alertDialog.dismiss();
                            }
                        });

                        greenImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int vValue = databaseHelper.getVvalue(view.getId());
                                int dValue = databaseHelper.getDvalue(view.getId());
                                if (vValue == 0 && dValue < 255){
                                    int newVValue = 1;
                                    publishMessage(newVValue, seekBarValue, topic);
                                    String sql = "update device set currentV =" + newVValue + ",";
                                    sql += "currentD =" + seekBarValue + "";
                                    sql += " where  topic='" + topic + "'";
                                    SQLiteDatabase db = databaseHelper.getWritableDatabase();
                                    db.execSQL(sql);

                                    Log.d("publishMessageMain", String.valueOf(newVValue)+" "+ String.valueOf(dValue)+ " " +topic);
                                }
                                alertDialog.dismiss();
                            }
                        });

                        alertDialog.show();
                    }

                }


            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                // Do nothing
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                // Image is centered to start, but we need to unhitch it to move it around.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    lp.removeRule(RelativeLayout.CENTER_HORIZONTAL);
                    lp.removeRule(RelativeLayout.CENTER_VERTICAL);
                } else {
                    lp.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
                    lp.addRule(RelativeLayout.CENTER_VERTICAL, 0);
                }

              /*  lp.leftMargin = X - _xDelta;
                lp.topMargin = Y - _yDelta;
                // Negative margins here ensure that we can move off the screen to the right
                // and on the bottom. Comment these lines out and you will see that
                // the image will be hemmed in on the right and bottom and will actually shrink.
                lp.rightMargin = view.getWidth() - lp.leftMargin - windowwidth;
                lp.bottomMargin = view.getHeight() - lp.topMargin - windowheight;*/
                if (width == windowwidth && height == windowheight) {
                } else {
                    view.animate()
                            .x(event.getRawX() + dX)
                            .y(event.getRawY() + dY)
                            .setDuration(0)
                            .start();

                    if (event.getRawX() + dX + width > windowwidth) {
                        view.animate()
                                .x(windowwidth - width)
                                .setDuration(0)
                                .start();
                    }
                    if (event.getRawX() + dX < 0) {
                        view.animate()
                                .x(0)
                                .setDuration(0)
                                .start();
                    }
                    if (event.getRawY() + dY + height > windowheight) {
                        view.animate()
                                .y(windowheight - height)
                                .setDuration(0)
                                .start();
                    }
                    if (event.getRawY() + dY < 0) {
                        view.animate()
                                .y(0)
                                .setDuration(0)
                                .start();
                    }
                    //view.setLayoutParams(lp);
                    return true;
                    // invalidate is redundant if layout params are set or not needed if they are not set.
//        mRrootLayout.invalidate();
                }

                return true;
        }
        return true;
    }

    public void publishMessage(int newVValue, int dValue, String topic){

        try {

            //{"V":0,"D":255}
            MqttMessage message = new MqttMessage();
            try   {
                JSONObject object = new JSONObject();
                object.put("V", newVValue);
                object.put("D", dValue);
                System.out.println("weSend"+object.toString());
                message.setPayload(String.valueOf(object).getBytes());
            } catch (Exception e){
                Log.d("Exception", e.toString());

            }

            //message.setPayload(payload.getBytes());
            client.publish(topic, message);
            Log.d("publishMessage", "Topic: "+topic+" || SubscriptionTopic: "+ topic + " || " + message);
            addToHistory("Message Published");
            if(!client.isConnected()){
                addToHistory(client.getBufferedMessageCount() + " messages in buffer.");
            }
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }
    private void addToHistory(String mainText){
        System.out.println("LOG: " + mainText);
        //    mAdapter.add(mainText);


    }
}
