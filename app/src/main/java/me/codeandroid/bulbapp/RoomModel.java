package me.codeandroid.bulbapp;

public class RoomModel {

    int id;
    String  roomNo, applianceNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getApplianceNo() {
        return applianceNo;
    }

    public void setApplianceNo(String applianceNo) {
        this.applianceNo = applianceNo;
    }
}
