package me.codeandroid.bulbapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.NamespaceContext;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder> {

    NavigationActivity navigationActivity;
    List<RoomModel> items = new ArrayList<>();

    public RoomAdapter(NavigationActivity navigationActivity, List<RoomModel> items){
        this.navigationActivity = navigationActivity;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.roomNoText.setText(String.valueOf(items.get(position).getRoomNo()));
        holder.applianceNoText.setText(String.valueOf(items.get(position).getApplianceNo()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView roomNoText, applianceNoText;

        public ViewHolder(View itemView) {
            super(itemView);

            roomNoText = (TextView) itemView.findViewById(R.id.roomNumberText);
            applianceNoText = (TextView) itemView.findViewById(R.id.applianceNumberText);
        }
    }
}
