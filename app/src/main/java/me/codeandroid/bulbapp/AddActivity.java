package me.codeandroid.bulbapp;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.bulbapp.database.DatabaseHelper;

public class AddActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    EditText editTextHouseName, editTextRoomNumber, editTextDeviceNumber;
    Button addButton;
    DatabaseHelper databaseHelper;
    Spinner spinner;
    String selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        editTextHouseName = (EditText)findViewById(R.id.houseNameEditText);
        editTextRoomNumber = (EditText)findViewById(R.id.roomNumberEditText);
        editTextDeviceNumber = (EditText)findViewById(R.id.deviceNumberEditText);
        addButton = (Button)findViewById(R.id.addButton);
        List<String> deviceType = new ArrayList<String>();

        databaseHelper = new DatabaseHelper(this);
        spinner = (Spinner)findViewById(R.id.deviceNameSpinner);

        spinner.setOnItemSelectedListener(this);

        deviceType.add("bulb");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, deviceType);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String houseName = editTextHouseName.getText().toString();
                String roomNumber = editTextRoomNumber.getText().toString();
                String deviceNumber = editTextDeviceNumber.getText().toString();

                if (houseName.length() == 0){
                    editTextHouseName.setError("House Name Cant be Blank");
                    return;
                }
                if (roomNumber.length() == 0){
                    editTextRoomNumber.setError("Room Number cant be blank");
                    return;
                }
                if (deviceNumber.length() == 0){
                    editTextDeviceNumber.setError("Device Number Cant be Blank");
                    return;
                }

                String query = "insert into device (deviceName, houseName, roomNumber, applianceNumber, topic) values ('"+selected+"', '"+houseName+"', '"+roomNumber+"', '"+deviceNumber+"', '"+houseName+"/"+roomNumber+"/"+deviceNumber+"')";
                SQLiteDatabase db = databaseHelper.getWritableDatabase();
                db.execSQL(query);
                Log.d("query", query);

               finish();
            }
        });

    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selected = parent.getItemAtPosition(position).toString();
        Log.d("selected", selected);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
