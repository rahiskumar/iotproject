package me.codeandroid.bulbapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.bulbapp.RoomModel;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static String DatabaseName = "TestApp.db";
    public static int DatabaseVersion = 1;

    String create_data_table = "create table data (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "deviceId INTEGER ," +
            "deviceName TEXT NOT NULL DEFAULT('0')," +
            "category TEXT NOT NULL DEFAULT('0'), " +
            "vValue INTEGER, " +
            "dValue INTEGER, " +
            "timeStamp TEXT )";

    String create_device_table = "create table device ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "deviceName TEXT, " +
            "houseName TEXT," +
            "roomNumber TEXT, " +
            "currentV INTEGER, " +
            "currentD INTEGER, " +
            "xPosition NUMERIC, " +
            "yPosition NUMERIC, " +
            "timeStamp TEXT, " +
            "applianceNumber TEXT, " +
            "topic TEXT)";

    Context context;

    public DatabaseHelper(Context context) {
        super(context, DatabaseName, null, DatabaseVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_data_table);
        Log.d("create_data_table", create_data_table);
        db.execSQL(create_device_table);
        Log.d("create_device_table", create_device_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public int getDevices() {
        int k = 0;
        String query = "select count(*) from device";
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null){
            if (cursor.moveToFirst()){
                k = cursor.getInt(0);
            }
        }
        return k;
    }

    public int getXposition(int i) {
        int xValue = 0;
        String query = "select xPosition from device where id = "+i+"";
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null){
            if(cursor.moveToFirst()){
                xValue = cursor.getInt(0);
                Log.d("xValue", String.valueOf(xValue));

            }
        }
        return xValue;
    }

    public int getYposition(int i) {
        int yValue = 0;
        String query = "select yPosition from device where id = "+i+"";
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null){
            if(cursor.moveToFirst()){
                yValue = cursor.getInt(0);
                Log.d("yValue", String.valueOf(yValue));
            }
        }
        return yValue;
    }

    public Cursor getAll() {
        String query = "select * from device";
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                do {
                    int id = cursor.getInt(cursor.getColumnIndex("id"));
                    String deviceName = cursor.getString(cursor.getColumnIndex("deviceName"));
                    float xPosition = cursor.getLong(cursor.getColumnIndex("xPosition"));
                    float yPosition = cursor.getLong(cursor.getColumnIndex("yPosition"));
                    Log.d("GetAllValues", String.valueOf(id)+ " " + deviceName + " "+ String.valueOf(xPosition)+" "+ String.valueOf(yPosition));
                }while (cursor.moveToNext());
            }
        }
        return  cursor;
    }

    public String getTopic(int i) {
        String topic = "";
        String query = "select topic from device where id ="+i;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null){
            if (cursor.moveToFirst()){
                topic = cursor.getString(0);
                Log.d("TopicInDBHelper", topic);
            }
        }
        return topic;
    }

    public int getVvalue(int id) {
        int result = 0;
        String query = "select currentV from device where id = "+id;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null){
            if (cursor.moveToFirst()){
                result = cursor.getInt(0);
            }
        }
        return result;
    }

    public int getDvalue(int id) {
        int result = 0;
        String query = "select currentD from device where id = "+id;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null){
            if (cursor.moveToFirst()){
                result = cursor.getInt(0);
            }
        }
        return result;
    }

    public List<RoomModel> getAllVall(){
        List<RoomModel> roomModelList = new ArrayList<>();
        String  query = "select * from device";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null){
                if (cursor.moveToFirst()){
                    do {

                        RoomModel roomModel = new RoomModel();
                        roomModel.setRoomNo(cursor.getString(cursor.getColumnIndex("roomNumber")));
                        roomModel.setApplianceNo(cursor.getString(cursor.getColumnIndex("applianceNumber")));

                        roomModelList.add(roomModel);

                    }while (cursor.moveToNext());
                }
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return roomModelList;
    }
    public String getHouseName(){
        String houseName="";
        String query = "select * from device";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null){
                if (cursor.moveToFirst()){
                    houseName = cursor.getString(cursor.getColumnIndex("houseName"));
                }
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return houseName;
    }
    public String getVValue(String topic){
        String  vValue = "";
        String query = "select currentV from device where topic='"+ topic + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        try {
            if (cursor != null){
                if (cursor.moveToFirst()){
                    vValue = cursor.getString(cursor.getColumnIndex("vValue"));
                }
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return vValue;
    }
}
